# Maintainer: Ultracoolguy <myownpersonalaccount@protonmail.com>
# Co-Maintainer: Azkali Manad <a_random_mailer@protonmail.com>
# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-nintendo-nx
pkgdesc="Nintendo Switch"
pkgver=0.1
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base linux-nintendo-nx joycond"
makedepends="devicepkg-dev"
hekate_version=5.5.4
nyx_version=1.0.1
hekate_bin="$srcdir/hekate_ctcaer_${hekate_version}.bin"
source="deviceinfo
	switch-l4t-configs.tar::https://gitlab.com/switchroot/switch-l4t-configs/-/archive/master/switch-l4t-configs.tar
	https://github.com/CTCaer/hekate/releases/download/v${hekate_version}-v2/hekate_ctcaer_${hekate_version}_Nyx_${nyx_version}.zip
	switch-configs.post-install"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware nintendo-nx-configs"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="Nvidia utilities + firmware. Probably won't work without it"
	depends="firmware-nintendo-nx"
	mkdir "$subpkgdir"
}

configs() {
	pkgdesc="Nintendo Switch configs"
	mkdir -p $subpkgdir/etc/systemd/system/ \
	$subpkgdir/etc/X11/xorg.conf.d/ \
	$subpkgdir/etc/dconf/db/local.d/ \
	$subpkgdir/etc/dconf/profile/ \
	$subpkgdir/etc/sddm.conf.d/ \
	$subpkgdir/etc/lightdm/lightdm.conf.d/ \
	$subpkgdir/etc/xdg/autostart/ \
	$subpkgdir/usr/bin/ \
	$subpkgdir/usr/lib/firmware \
	$subpkgdir/usr/lib/udev/rules.d/ \
	$subpkgdir/usr/share/onboard/ \
	$subpkgdir/usr/lib/systemd/system-sleep \
	$subpkgdir/usr/lib/systemd/system/bluetooth.service.d/ \
	$subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/ \
	$subpkgdir/var/lib/alsa/

	# Hekate bin
	install ${hekate_bin} $subpkgdir/usr/lib/firmware/reboot_payload.bin

	tar xf switch-l4t-configs.tar
	mv switch-l4t-configs-* switch-l4t-configs

	# Dock-hotplug
	install switch-l4t-configs/switch-dock-handler/92-dp-switch.rules $subpkgdir/usr/lib/udev/rules.d/
	install switch-l4t-configs/switch-dock-handler/dock-hotplug $subpkgdir/usr/bin/
	install switch-l4t-configs/switch-dock-handler/nintendo-switch-display.desktop $subpkgdir/etc/xdg/autostart/
	sed 's/sudo -u/sudo -s -u/g' -i $subpkgdir/usr/bin/dock-hotplug

	# Dconf customizations
	install switch-l4t-configs/switch-dconf-customizations/99-switch $subpkgdir/etc/dconf/db/local.d/
	install switch-l4t-configs/switch-dconf-customizations/user $subpkgdir/etc/dconf/profile/

	# Alsa UCM2 profile
	install switch-l4t-configs/switch-alsa-ucm2/tegra-snd-t210ref-mobile-rt565x.conf $subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/
	install switch-l4t-configs/switch-alsa-ucm2/HiFi.conf $subpkgdir/usr/share/alsa/ucm2/tegra-snd-t210ref-mobile-rt565x/
	install switch-l4t-configs/switch-alsa-ucm2/asound.state $subpkgdir/var/lib/alsa/

	# Bluetooth service
	install switch-l4t-configs/switch-bluetooth-service/switch-bluetooth.service $subpkgdir/usr/lib/systemd/system/

	# Lightdm conf
	install switch-l4t-configs/switch-lightdm-conf/10-dock-hotplug.conf $subpkgdir/etc/lightdm/lightdm.conf.d/
	install switch-l4t-configs/switch-lightdm-conf/20-onboard.conf $subpkgdir/etc/lightdm/lightdm.conf.d/

	# Onboard conf
	install switch-l4t-configs/switch-onboard-conf/onboard-default-settings.gschema.override.example $subpkgdir/usr/share/onboard/

	# Touchscreen rules
	install switch-l4t-configs/switch-touch-rules/99-switch-touchscreen.rules $subpkgdir/usr/lib/udev/rules.d/
	
	# System Suspend-modules
	install -Dm 0755 switch-l4t-configs/switch-suspend-modules/suspend-modules $subpkgdir/usr/lib/systemd/system-sleep/
	install switch-l4t-configs/switch-suspend-modules/suspend-modules.conf $subpkgdir/etc/

	# SDDM rule
	install switch-l4t-configs/switch-sddm-rule/69-nvidia-seat.rules $subpkgdir/usr/lib/udev/rules.d/

	# R2P service
	install switch-l4t-configs/switch-r2p-service/r2p.service $subpkgdir/etc/systemd/system/

	# Upower service
	install switch-l4t-configs/switch-upower/upower.service $subpkgdir/etc/systemd/system/
	sed -i 's/libexec/lib/g' $subpkgdir/etc/systemd/system/upower.service

	# Xorg conf
	install switch-l4t-configs/switch-xorg-conf/10-monitor.conf $subpkgdir/etc/X11/xorg.conf.d/
	install switch-l4t-configs/switch-xorg-conf/50-joycons.conf $subpkgdir/etc/X11/xorg.conf.d/

	echo "xrandr --dpi 96" > xprofile
	install xprofile $subpkgdir/etc/xprofile
}

sha512sums="ca61caca7be42dab4dd83e797407ad31a5dbcfccc5d8bf04ee612a58a311899bdc937e3ac0a558f240119de261e4c5fa3085539857d3df47ede997c305b3afa6  deviceinfo
ae8c735960e4c50e96dad22ced60c92a6b08bb5241e43369e8a759437d22e509268c144d0ff2f0d203782fa4df7415494907b5a995c5e8f0938a33eedd6fdaf0  switch-l4t-configs.tar
d1a219e9a36625731dc874dcda2ec40ebba4b67ecb217da5acf6b9916f7261deeefd017f6056621a8b267879208186ca4f913c9d045a388f898c608831126db5  hekate_ctcaer_5.5.4_Nyx_1.0.1.zip
e9df16f2555c1d72caf8ff0f297590dddd082d9e9c67e68f39ea040268a7b2b6f988960d75d94499222bcef78f024003451cb035621dfabe24ca7c5beb54ba09  switch-configs.post-install"
